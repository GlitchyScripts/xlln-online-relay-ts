import { Socket, createSocket } from "dgram";
import { AddressInfo } from "net";

// UDP IPv4
let server: Socket = createSocket('udp4');

server.on('listening', function (): void
{
    let address: AddressInfo = <AddressInfo>server.address();
    console.log(`XLLN UDP Server listening on ${address.address}:${address.port}.`);
})
.on('message', UdpPacketReceived);

const XLLN_CUSTOM_PACKET_SENTINEL = 0x00;
const XLLNCustomPacketType = {
    UNKNOWN: 0x00,
    STOCK_PACKET: 0x01,
    STOCK_PACKET_FORWARDED: 0x02,
    CUSTOM_OTHER: 0x03,
    UNKNOWN_USER_ASK: 0x04,
    UNKNOWN_USER_REPLY: 0x05,
    LIVE_OVER_LAN_ADVERTISE: 0x06,
    LIVE_OVER_LAN_UNADVERTISE: 0x07,
}

const XLLNOnlineCustomPacketType = {
    UNKNOWN: 0x00,
    REQ_IPv4: 0x01,
    REQ_IPv4_REPLY: 0x02,
    HOLE_PUNCH_REGISTER: 0x03,
    HOLE_PUNCH_REQUEST: 0x04,
    HOLE_PUNCH_RESPONSE: 0x05,
    NAT_CHECK: 0x06,
}

interface XllnClientData {
    ipv4: string;
    port: number;
    lastComm: number;
}

let xlln_clients = {};
//xlln_clients[["127.0.0.1", 1001].join(':')] = {ipv4:"127.0.0.1", port:1001, lastComm:(new Date).getTime()};

interface XllnPortMapItem {
    ipv4: string;
    port_external: number;
    port_internal: number;
    lastComm: number;
}

let xlln_port_mappings_external_to_internal = {};
let xlln_port_mappings_internal_to_external = {};

let packetHeadXLLNOnline: number[] = [XLLN_CUSTOM_PACKET_SENTINEL, XLLNCustomPacketType.CUSTOM_OTHER, 'X'.charCodeAt(0), 'O'.charCodeAt(0)];
let packetHeadXLLNOnlineBuf: Buffer = Buffer.from(packetHeadXLLNOnline);

let handle_RemoveOldClients: NodeJS.Timeout = null;

let timeout_after_ms: number = 60 * 1000;

let PORT: number = 6001;
//let HOST: string = '127.0.0.1';
server.bind(PORT);

function RemoveOldClients(): void
{
    let timeNow: number = (new Date).getTime();
    let oldClients: string[] = [];
    for (let xlln_client of Object.keys(xlln_clients)) {
        let xlln_data: XllnClientData = xlln_clients[xlln_client];
        if (xlln_data.lastComm < timeNow - (timeout_after_ms + 500)) {
            oldClients.push(xlln_client);
            delete xlln_clients[xlln_client];
        }
    }
    if (oldClients.length > 0) {
        console.log(`Removed Old Clients: ${oldClients.join(", ")}.`);
    }
    handle_RemoveOldClients = null;
}

function GetSegment(msg: any, start_index: number, length: number): any
{
    return msg.slice(start_index, start_index + length);
}

function ToPaddedHexString(num: number, len: number, OhExPrefix: boolean = false) {
    let str: string = num.toString(16);
    return (OhExPrefix ? "0x" : "") + "0".repeat(len - str.length) + str;
}

function UdpPacketReceived(msg: Buffer, remote_info: AddressInfo): void
{
    if (msg.length <= 0) {
        return;
    }
    let user_addr = [remote_info.address, remote_info.port].join(':');
    let user_data: XllnClientData = xlln_clients[user_addr];
    if (user_data === undefined) {
        user_data = {ipv4:remote_info.address, port:remote_info.port, lastComm:undefined};
        xlln_clients[user_addr] = user_data;
    }
    user_data.lastComm = (new Date).getTime();
    
    if (msg[0] !== XLLN_CUSTOM_PACKET_SENTINEL || (msg[0] === XLLN_CUSTOM_PACKET_SENTINEL && (msg[1] === XLLNCustomPacketType.STOCK_PACKET || msg[1] === XLLNCustomPacketType.STOCK_PACKET_FORWARDED))) {
        // It is a stock packet.
        
        // TODO wrap it and make a new XLLNCustomPacketType type: STOCK_PACKET_FORWARDED
        // that has an XNADDR prepended.
        console.log(`STOCK.`);
    }
    else {
        // It is a custom packet.
        switch(msg[1]) {
            case XLLNCustomPacketType.CUSTOM_OTHER: {
                if (msg.slice(0, packetHeadXLLNOnline.length).equals(packetHeadXLLNOnlineBuf)) {
                    delete xlln_clients[user_addr];
                    const packetTypeHex = ("00" + GetSegment(msg, packetHeadXLLNOnline.length, 1).toString('hex')).substr(-2);
                    switch (msg[packetHeadXLLNOnline.length]) {
                        case XLLNOnlineCustomPacketType.REQ_IPv4: {
                            let client_version = Number.parseInt('0x'+GetSegment(msg, packetHeadXLLNOnline.length + 1, 4).toString('hex'));
                            if (Number.isNaN(client_version)) {
                                client_version = 0;
                            }
                            if (client_version >= 1) {
                                console.log(`XLLNOnline New Client Version: ${client_version}.`);
                                const hIpv4: number[] = remote_info.address.split('.').map((value: string) => (Number.parseInt(value))).reverse();
                                const hPort: number[] = [remote_info.port & 0xFF, remote_info.port >> 8];
                                server.send(Buffer.from(packetHeadXLLNOnline.concat([XLLNOnlineCustomPacketType.REQ_IPv4_REPLY]).concat(hIpv4).concat(hPort)), remote_info.port, remote_info.address);
                            }
                            else {
                                console.log(`XLLNOnline Old Client Version: ${client_version}.`);
                            }
                            break;
                        }
                        case XLLNOnlineCustomPacketType.HOLE_PUNCH_REGISTER: {
                            // nPort input
                            const desired_port: number = Number.parseInt('0x'+GetSegment(msg, packetHeadXLLNOnline.length + 1, 2).reverse().toString('hex'));
                            console.log(`XLLNOnline HolePunch Register: ${remote_info.address}:(${desired_port}->${remote_info.port}).`);
                            xlln_port_mappings_internal_to_external[[remote_info.address, desired_port].join(':')] = xlln_port_mappings_external_to_internal[[remote_info.address, remote_info.port].join(':')] = {ipv4:"127.0.0.1", port_external:remote_info.port, port_internal:desired_port, lastComm:(new Date).getTime()};
                            break;
                        }
                        case XLLNOnlineCustomPacketType.HOLE_PUNCH_REQUEST: {
                            const hIpv4_sender: number[] = remote_info.address.split('.').map((value: string) => (Number.parseInt(value)));
                            const hPort_external_sender: number[] = [remote_info.port & 0xFF, remote_info.port >> 8];
                            
                            // nPort input
                            const hPort_internal_sender: number[] = Array.from(GetSegment(msg, packetHeadXLLNOnline.length + 1, 2));
                            const port_internal_sender: number = Number.parseInt("0x" + hPort_internal_sender.map((value: number) => (ToPaddedHexString(value, 2))).join(""));
                            // nIP input
                            const hIpv4_target: number[] = Array.from(GetSegment(msg, packetHeadXLLNOnline.length + 1 + 2, 4));
                            const ip_target: string = hIpv4_target.reverse().map((value) => (value.toString(10))).join('.');
                            // nPort input
                            const hPort_internal_target: number[] = Array.from(GetSegment(msg, packetHeadXLLNOnline.length + 1 + 2 + 4, 2));
                            const port_internal_target: number = Number.parseInt("0x" + hPort_internal_target.map((value: number) => (ToPaddedHexString(value, 2))).join(""));
                            
                            let portmap_target: XllnPortMapItem = xlln_port_mappings_internal_to_external[[ip_target, port_internal_target].join(':')];
                            if (portmap_target === undefined) {
                                portmap_target = {ipv4:"127.0.0.1", port_external:0, port_internal:0, lastComm:(new Date).getTime()};
                            }
                            
                            console.log(`XLLNOnline HolePunch Request: Target: ${ip_target}:${port_internal_target}->${portmap_target.port_external}. Sender: ${remote_info.address}:${port_internal_sender}->${remote_info.port}.`);
                            if (ip_target === "" || port_internal_target === 0) {
                                console.log(`ERROR: INVALID HolePunch Request.`);
                                break;
                            }
                            
                            const hPort_external_target: number[] = [portmap_target.port_external & 0xFF, portmap_target.port_external >> 8];
                            
                            // To Target: Target's Internal Port, Sender IP, Sender External Port.
                            server.send(Buffer.from(packetHeadXLLNOnline.concat([XLLNOnlineCustomPacketType.HOLE_PUNCH_REQUEST]).concat(hPort_internal_target).concat(hIpv4_sender).concat(hPort_external_sender)), portmap_target.port_external, ip_target);
                            // To Sender: Sender's Internal Port, Target IP, Target External Port.
                            server.send(Buffer.from(packetHeadXLLNOnline.concat([XLLNOnlineCustomPacketType.HOLE_PUNCH_REQUEST]).concat(hPort_internal_sender).concat(hIpv4_target).concat(hPort_external_target)), remote_info.port, remote_info.address);
                            break;
                        }
                        case XLLNOnlineCustomPacketType.HOLE_PUNCH_RESPONSE: {
                            console.log(`ERROR: INVALID HolePunch Response. Not designed to be received by the relay.`);
                            break;
                        }
                        case XLLNOnlineCustomPacketType.NAT_CHECK: {
                            console.log(`XLLNOnline NAT Check: ${remote_info.address}:${remote_info.port}.`);
                            let serverRand: Socket = createSocket('udp4');
                            /*serverRand.on('listening', function (): void
                            {
                                let address: AddressInfo = <AddressInfo>serverRand.address();
                                console.log(`XLLN UDP Rand Server listening on ${address.address}:${address.port}.`);
                            });*/
                            const hIpv4: number[] = remote_info.address.split('.').map((value: string) => (Number.parseInt(value))).reverse();
                            const hPort: number[] = [remote_info.port & 0xFF, remote_info.port >> 8];
                            serverRand.send(Buffer.from(packetHeadXLLNOnline.concat([XLLNOnlineCustomPacketType.NAT_CHECK]).concat(hIpv4).concat(hPort)), remote_info.port, remote_info.address, () => {
                                serverRand.close();
                            });
                            break;
                        }
                        default: {
                            server.send(Buffer.from(packetHeadXLLNOnline.concat([XLLNOnlineCustomPacketType.UNKNOWN, msg[packetHeadXLLNOnline.length]])), remote_info.port, remote_info.address);
                            console.log(`ERROR: UNKNOWN XLLNOnline Packet from ${user_addr} - ${packetTypeHex}.`);
                            return;
                        }
                    }
                    console.log(`XLLNOnline Packet from ${user_addr} - ${packetTypeHex}.`);
                    return;
                }
                console.log(`XLLN CUSTOM_OTHER from ${user_addr} - ${msg.slice(2).toString('hex')}.`);
                break;
            }
            case XLLNCustomPacketType.UNKNOWN_USER_REPLY: {
                // Ignore & Discard.
                console.log(`XLLN UNKNOWN_USER_REPLY from ${user_addr}.`);
                return;
            }
            case XLLNCustomPacketType.UNKNOWN_USER_ASK: {
                const XNADDR_SIZE: number = 36;
                console.log(`XLLN UNKNOWN_USER_ASK from ${user_addr}.`);
                console.log(`Original Packet: ${msg.slice(2+XNADDR_SIZE).toString('hex')}.`);
                const Ipv4Address: string = "45.76.112.15";
                const portOnline: number = 6000;
                const nIpv4: number[] = Ipv4Address.split('.').map((value: string) => (Number.parseInt(value)));
                const nPort: number[] = [portOnline >> 8, portOnline & 0xFF];
                // XNADDR struct is {ina, inaOnline, wPortOnline, abEnet, abOnline}.
                server.send(Buffer.concat([Buffer.from([XLLN_CUSTOM_PACKET_SENTINEL, XLLNCustomPacketType.UNKNOWN_USER_REPLY].concat(nIpv4).concat(new Array(4).fill(0)).concat(nPort).concat(new Array(6).fill(0)).concat(new Array(20).fill(0))), msg.slice(2+XNADDR_SIZE)]), remote_info.port, remote_info.address);
                return;
            }
            default: {
                const typeAsTxt: String = msg.length > 1 ? ("00" + msg[1].toString(16)).substr(-2) : "NULL";
                console.log(`XLLN TYPE from ${user_addr} - ${typeAsTxt}.`);
                break;
            }
        }
        
    }
    
    let send_list: string[] = [];
    
    for (let xlln_client of Object.keys(xlln_clients)) {
        const xlln_data: XllnClientData = xlln_clients[xlln_client];
        if (xlln_client === user_addr) {
            continue;
        }
        server.send(msg, xlln_data.port, xlln_data.ipv4);
        send_list.push(xlln_client);
    }
    
    console.log(`${user_addr} [${msg.subarray(0, 2).toString('hex')}] -> ${send_list.join(', ')}.`);
    if (handle_RemoveOldClients === null) {
        handle_RemoveOldClients = setTimeout(RemoveOldClients, timeout_after_ms);
        console.log(`setTimeout(${timeout_after_ms}, RemoveOldClients).`);
    }
}
