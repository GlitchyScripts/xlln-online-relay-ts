# xlln-online-relay
A typescript server for relaying broadcast and other network packets sent/redirected by an XLLN Module like xlln-online.dll ( https://gitlab.com/GlitchyScripts/xlln-modules ).
